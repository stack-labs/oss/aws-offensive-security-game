# Mission 3 cheat sheet


This mission serves to introduce users to snapshots, EBS volumes and mounting.


  * Given: Instance ip, SSH key
  * Variables: $id, $instance_ip

Do you know how to list the EC2 present in the same region?
https://docs.aws.amazon.com/cli/latest/reference/ec2/describe-instances.html

Do you fine the owner-id ?

Do you know how to list the EC2 snapshot present on a EC2?\
https://docs.aws.amazon.com/cli/latest/reference/ec2/describe-snapshots.html\

Do you know how to create an EBS volume with snapshot_id ?\
https://docs.aws.amazon.com/cli/latest/reference/ec2/create-volume.html\

## Route 1: Mount the volume on the current instance

https://docs.aws.amazon.com/fr_fr/AWSEC2/latest/UserGuide/ebs-attaching-volume.html

https://doc.ubuntu-fr.org/mount_fstab \
https://access.redhat.com/documentation/fr-fr/red_hat_enterprise_linux/6/html/storage_administration_guide/xfsmounting \ 

## Route 2: Create a new instance and mount the volume there

Create your SSH-key for the EC2 instance.

https://help.github.com/en/github/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent

https://docs.aws.amazon.com/cli/latest/reference/opsworks/create-instance.html

https://doc.ubuntu-fr.org/mount_fstab \
https://access.redhat.com/documentation/fr-fr/red_hat_enterprise_linux/6/html/storage_administration_guide/xfsmounting \ 

## With the volume mounted

You win! Note that the instance has the required privileges to generate administrator AWS keys...


## Remember

**Delete anything you created before attempting to destroy the mission. This includes volumes, instances, and so on. The snapshot is destroyed by the script, so do not worry about that.**