# Mission 4 cheat sheet


This mission lets users manipulate new EC2 instances in order to become more familiar with security groups, and has a small IAM component.


  * Given: AWS key, AWS secret key, ssh key
  * Variables: $id

Did you configure the juan profile ?

Do you know how to list the EC2 present in the same region?
https://docs.aws.amazon.com/cli/latest/reference/ec2/describe-instances.html


## Running commands from the instance

Do you know how to list the EC2 security group ?
Do you know how to list the EC2 present in the same region?

## Alternative: Get the credentials and load them on your own shell

Did you know if you can create an EC2 instance ?
Did you check the metadata instance ?


## Remember

**Delete anything you created before attempting to destroy the mission, especially the new instance! You can, of course, use the console or the CLI for it with your admin profile.**