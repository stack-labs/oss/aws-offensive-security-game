# Mission 2 cheat sheet


This mission is focused on using the metadata server.


  * Given: Instance ip, user and password
  * Variables: $id, $instance_ip


$ `ssh eviluser@$instance_ip`

Log into the instance to look around.


## Route 1: Use the metadata server to get the key

Did you know wath is the ip of the metadata server IMDSv1 and wath is the /user-data ?\
	https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instancedata-data-retrieval.html
	https://docs.aws.amazon.com/fr_fr/AWSEC2/latest/UserGuide/user-data.html


## Route 2: Look around in the linux system to get the key

Have you looked at your UNIX user's rights?


## Once the SSH key is obtained

Do you know how to list the EC2 present in the same region?
https://docs.aws.amazon.com/cli/latest/reference/ec2/describe-instances.html
