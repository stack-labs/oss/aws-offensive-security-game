# Mission 1 cheat sheet


This mission is mainly used to show that a public bucket is a bad, bad idea.


  * Given: Bucket name
  * Variables: $id

Did you try to list bucket with 'aws S3 ls'?

Did you try to synchronize bucket with 'aws S3 sync'?

Do you know what a .git is and what 'git log' and 'git checkout' commands do?
