#!/bin/bash

echo "[SauerCloud]Do you have the version of terraform superior to version 0.12.19 ? (y/n) "
read -r answer
if [[ "$answer" == "n" ]]
then
		if [[ "$OSTYPE" == "linux-gnu" ]]; then
        		OS="linux"
		elif [[ "$OSTYPE" == "darwin"* ]]; then
				OS="darwin"
		#elif [[ "$OSTYPE" == "cygwin" ]]; then
				# POSIX compatibility layer and Linux environment emulation for Windows
		#elif [[ "$OSTYPE" == "msys" ]]; then
				# Lightweight shell and GNU utilities compiled for Windows (part of MinGW)
		elif [[ "$OSTYPE" == "win32" ]]; then
				OS="windows"
		elif [[ "$OSTYPE" == "freebsd"* ]]; then
				OS="freebsd"
		else
				# Unknown.
			echo 'Unsupported OS'
			exit 1
		fi
		TERRAFORM_VERSION='0.12.24'
		TERRAFORM_FILE_NAME=terraform_${TERRAFORM_VERSION}_${OS}_amd64.zip
		curl -O -L https://releases.hashicorp.com/terraform/$TERRAFORM_VERSION/$TERRAFORM_FILE_NAME
		mkdir tmp
		unzip $TERRAFORM_FILE_NAME -d tmp
		rm $TERRAFORM_FILE_NAME
		#export TERRAFORM=$(pwd)/tmp/terraform
		if [[ "$OS" == "linux" ]]; then 
			echo "alias terraform='$(pwd)/tmp/terraform'">> ~/.bashrc
			source ~/.bashrc
		elif [[ "$OS" == "darwin" ]]; then 
		   	echo "alias terraform='$(pwd)/tmp/terraform'">> ~/.bash_profile
			source ~/.bash_profile
		else
			alias terraform='$(pwd)/tmp/terraform'

		fi 
		echo tmp/terraform >> .gitignore 
	echo "[SauerCloud] Now you have terraform 0.12.24 "
fi

echo "[SauerCloud] Enter Your Level in Security ( be careful not to change the level along the game ) script_kiddie  / hackeur / pro_hackeur :( sk / h / ph )"
read -r SAUERCLOUD_LEVEL
if [[ -z "$SAUERCLOUD_LEVEL" ]]
then
	echo "[SauerCloud] Error, no Level specified."
	echo "[SauerCloud] Abort."
	exit
fi

if [[ "$SAUERCLOUD_LEVEL" == "sk" || "$SAUERCLOUD_LEVEL" == "script_kiddie" ]]
then
	echo "[SauerCloud] You choose the level: script_kiddie "
	if [[ -e level.txt ]]
	then
		echo "[SauerCloud] File 'level.txt' already exists. Overwrite? (y/n)"
		read -r answer
		if [[ "$answer" == "y" ]]
		then
			rm level.txt
			touch level.txt
			echo "script_kiddie" >> level.txt
		fi
	else
		touch level.txt
		echo  "script_kiddie" >> level.txt
	fi
fi
if [[ "$SAUERCLOUD_LEVEL" == "h" || "$SAUERCLOUD_LEVEL" == "hackeur" ]]
then
	echo "[SauerCloud] You choose the level: hackeur "
	if [[ -e level.txt ]]
	then
		echo "[SauerCloud] File 'level.txt' already exists. Overwrite? (y/n)"
		read -r answer
		if [[ "$answer" == "y" ]]
		then
			rm level.txt
			touch level.txt
			echo "hackeur" >> level.txt
		fi
	else
		touch level.txt
		echo  "hackeur" >> level.txt
	fi
fi
if [[ "$SAUERCLOUD_LEVEL" == "ph" || "$SAUERCLOUD_LEVEL" == "pro_hackeur" ]]
then
	echo "[SauerCloud] You choose the level: pro_hackeur "
	if [[ -e level.txt ]]
	then
		echo "[SauerCloud] File 'level.txt' already exists. Overwrite? (y/n)"
		read -r answer
		if [[ "$answer" == "y" ]]
		then
			rm level.txt
			touch level.txt
			echo "pro_hackeur" >> level.txt
		fi
	else
		touch level.txt
		echo  "pro_hackeur" >> level.txt
	fi
fi
echo "[SauerCloud] Configuration level complete."
echo "[SauerCloud] Enter the name of your default AWS profile that will create all challenges:"
read -r SAUERCLOUD_USER_PROFILE
if [[ -z "$SAUERCLOUD_USER_PROFILE" ]]
then
	echo "[SauerCloud] Error, no profile specified."
	echo "[SauerCloud] Abort."
	exit
fi
if [[ -e profile.txt ]]
then
	echo "[SauerCloud] File 'profile.txt' already exists. Overwrite? (y/n)"
	read -r answer
	if [[ "$answer" == "y" ]]
	then
		rm profile.txt
		touch profile.txt
		echo "$SAUERCLOUD_USER_PROFILE" >> profile.txt
	fi
else
	touch profile.txt
	echo  "$SAUERCLOUD_USER_PROFILE" >> profile.txt
fi
echo "[SauerCloud] $SAUERCLOUD_USER_PROFILE will be your default profile."
echo "[SauerCloud] SauerCloud can fetch your IP address directly and add it to the whitelist by cURL-ing \"icanhazip.com\". Do you wish to proceed? (y/N)"
read -r ans
if [[ ! "$ans" == "y" ]]
then
	echo "[SauerCloud] You may want to create a \"whitelist.txt\" file with your IP address in order to facilitate deployment."
	exit 2
fi	
echo "[SauerCloud] Fetching and whitelisting IP..."
if [[ -e "whitelist.txt" ]]
then
	echo "[SauerCloud] File 'whitelist.txt' already exists. Overwrite? (y/n)"
	read -r answer
	if [[ "$answer" == "y" ]]
	then
		rm whitelist.txt
		touch whitelist.txt
	fi
else
	touch whitelist.txt
fi
curl -f -s checkip.amazonaws.com >>  whitelist.txt
echo -n "[SauerCloud] Your ip is "; cat whitelist.txt
echo "[SauerCloud] Configuration complete."


